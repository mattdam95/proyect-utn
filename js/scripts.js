const spinner = document.querySelector("#spinner");
const formulario = document.querySelector("#enviar-email");
const btnEnviar = document.querySelector("#btn-enviar");

function Over(serv) {
    var boton = document.getElementById(serv);
    boton.style.backgroundColor = "#707070";
    boton.style.color = "white";
}

function Transparent(serv2) {
    var boton = document.getElementById(serv2);
    boton.style.backgroundColor = "#fcdc64";
    boton.style.color = "rgb(54,54,54)";
}

var text = document.getElementsByTagName("h4");

function mostrartext(id) {
    var selector = document.getElementById(id);
    console.log(selector);

    if (selector == serv1) {
        text[0].style.display = "flex";
    } else if (selector == serv2) {
        text[1].style.display = "flex";
    } else if (selector == serv3) {
        text[2].style.display = "flex";
    } else {
        text[3].style.display = "flex";
    }
}

function mostrartitulo(id) {
    var text = document.getElementById(id);
    text.style.display = "block";
}

function ocultar(id) {
    var text = document.getElementById(id);
    text.style.display = "none";
}

function BlurIn(id) {
    var blur = document.getElementById(id);
    blur.style.filter = "blur(2px)";
    blur.style.transition = "1s";
}

function BlurOut(id) {
    var blur = document.getElementById(id);
    blur.style.filter = "blur(0px)";
    blur.style.transition = "1s";
}

function Cambiar(id, nombre) {
    var heading = document.getElementById(id);
    heading.textContent = "@" + nombre;
}

function NoCambiar(id, nombre) {
    var heading = document.getElementById(id);
    heading.textContent = nombre;
}

formulario.addEventListener("submit", enviarEmail);

function enviarEmail(e) {
    e.preventDefault();
    btnEnviar.classList.add("visually-hidden");
    spinner.classList.remove("visually-hidden");

    setTimeout(() => {
        spinner.classList.add("visually-hidden");
        var toastElList = [].slice.call(document.querySelectorAll(".toast"));
        var toastList = toastElList.map(function(toastEl) {
            return new bootstrap.Toast(toastEl);
        });
        toastList.forEach((toast) => toast.show());

        setTimeout(() => {
            btnEnviar.classList.remove("visually-hidden");
        }, 5500);
    }, 3000);
}